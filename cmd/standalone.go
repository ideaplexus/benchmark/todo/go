package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"regexp"
	"strings"
)

var todos []Todo

// net/http based router
type route struct {
	pattern *regexp.Regexp
	verb    string
	handler http.Handler
}

type RegexpHandler struct {
	routes []*route
}

func (h *RegexpHandler) Handler(pattern *regexp.Regexp, verb string, handler http.Handler) {
	h.routes = append(h.routes, &route{pattern, verb, handler})
}

func (h *RegexpHandler) HandleFunc(r string, v string, handler func(http.ResponseWriter, *http.Request)) {
	re := regexp.MustCompile(r)
	h.routes = append(h.routes, &route{re, v, http.HandlerFunc(handler)})
}

func (h *RegexpHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	for _, route := range h.routes {
		if route.pattern.MatchString(r.URL.Path) && route.verb == r.Method {
			route.handler.ServeHTTP(w, r)
			return
		}
	}
	http.NotFound(w, r)
}

//
func jsonResponse(res http.ResponseWriter, data interface{}) {
	res.Header().Set("Content-Type", "application/json; charset=utf-8")

	payload, err := json.Marshal(data)
	if errorCheck(res, err) {
		return
	}

	fmt.Fprintf(res, string(payload))
}

func errorCheck(res http.ResponseWriter, err error) bool {
	if err != nil {
		http.Error(res, err.Error(), http.StatusInternalServerError)
		return true
	}
	return false
}

type Server struct {
}

type Todo struct {
	Id      int    `json:"id"`
	Content string `json:"content"`
	State   string `json:"state"`
}

func main() {
	server := &Server{}
	handler := new(RegexpHandler)

	handler.HandleFunc("/tasks", "GET", server.listTasks)
	handler.HandleFunc("/tasks", "POST", server.addTask)

	if err := http.ListenAndServe(":3000", handler); err != nil {
		panic(err)
	}
}

func filterTasks(vs []Todo, s string) []Todo {
	vsf := make([]Todo, 0)
	for _, v := range vs {
		if strings.EqualFold(v.State, s) {
			vsf = append(vsf, v)
		}
	}
	return vsf
}

func (s *Server) listTasks(res http.ResponseWriter, req *http.Request) {
	q := req.URL.Query()
	state := q.Get("state")
	jsonResponse(res, filterTasks(todos, state))
}

func (s *Server) addTask(res http.ResponseWriter, req *http.Request) {
	todo := Todo{}

	decoder := json.NewDecoder(req.Body)
	err := decoder.Decode(&todo)
	if err != nil {
		fmt.Println("ERROR decoding JSON - ", err)
		return
	}
	defer req.Body.Close()

	todos = append(todos, todo)

	jsonResponse(res, todo)
}
