.PHONY: help all build build-compress clean fix vendor

APP_NAME    := standalone
BUILD_DIR   := build
MAIN_FILE   := cmd/$(APP_NAME).go
OUTPUT_FILE := $(BUILD_DIR)/$(APP_NAME)

help: ## Show this help.
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'

all: vendor fix

build: ## Builds the application
	go build -ldflags='-s -w' -mod vendor -i -v -o ${OUTPUT_FILE} ${MAIN_FILE}

build-compress: build
	upx --brute ${OUTPUT_FILE}

clean: ## Cleans the project.
	go clean -mod vendor -v

fix: ## Fixes any coding style issues in the source files.
	go fmt ./cmd/...

vendor: ## Creates or updates the vendor directory
	go mod tidy -v
	go mod vendor -v
